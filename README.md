This is my emacs configuration. A couple of features in my configuration are:

i)   auto complete.
ii)  vim like modeline.
iii) full-screen mode
iv)  easy buffer navigation
v)   Other basic customizations like full screen, startup screen inhibited, linum mode activated, blink cursor mode set to false        etc.    

As for the 1st commit, the CEDET features include in the .emacs.d directory are not utilised.

   	   	   		   		   	   		 Yo emacs!!!